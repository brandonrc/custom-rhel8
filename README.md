# custom-rhel8

## Create Custom Image
Going to use `lorax-composer`

How to? 

### HOW TO


## Customise a RAW IMAGE

### Setup Tools

#### ---- CentOS / RHEL / RockyOS ---
```
$ sudo yum -y install libguestfs-tools
```
#### ---- Ubuntu / Debian ----

```
$ sudo apt-get -y install libguestfs-tools
```

#### ---- Arch / Manjaro ----
```
yaourt -S --noconfirm --needed libguestfs
```

### HELP
Always good to remember how to ask for help.
```
virtu-customize --help
```

### Customize
First, access set LIBGUIESTFS_BACKEND to direct
```
export LIBGUESTFS_BACKEND=direct
```

#### Password
```
virt-customize -a rhel-server-7.6.qcow2 --root-password password:StrongRootPassword
```

#### Command
Here is an example of running a command to run the subscription-manager.  
```
virt-customize -a overcloud-full.qcow2 --run-command 'subscription-manager register --username=[username] --password=[password]'
```

#### Upload
```
virt-customize -a rhel-server-7.6.qcow2 --upload rhsm.conf:/etc/rhsm/rhsm.conf
```

#### Install softawre packages
```
virt-customize -a rhel-server-7.6.qcow2 --install [vim,bash-completion,wget,curl,telnet,unzip]
```